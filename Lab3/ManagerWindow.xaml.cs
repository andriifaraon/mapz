﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace BeautySalonMAPZ
{
    /// <summary>
    /// Interaction logic for ManagerWindow.xaml
    /// </summary>
    public partial class ManagerWindow : Window
    {
        Boss boss;
        Manager manager;
        MasterCollection master_collection;
        MasterHistory master_history;
        AppointmentCollection appointment_collection;
        DateTime start_session;
        MoneyReport money_report;
        private readonly string access_password;
        private const string access_money_password = "4321";
        private double money = 0;
        public ManagerWindow()
        {
            InitializeComponent();
            SetComboBoxes();
        }
        public ManagerWindow( string name, string surname, string pass )
        {
            InitializeComponent();
            SetComboBoxes();
            access_password = pass;
            SetData( name, surname, pass );            
        }
        private void SetComboBoxes()
        {
            SetMonth();
            SetDays();
            SetHours();
            SetMinute();
        }
        private void SetMonth()
        {
            for (int i = 0; i < 12; i++)
            {
                if (i >= DateTime.Now.Month - 1)
                    MonthManComboBox.Items.Add((int)(Months)Enum.GetValues(typeof(Months)).GetValue(i) + 1);
            }
        }
        private void SetDays()
        {
            for (int i = 1; i <= 31; i++)
                DayManComboBox.Items.Add(i.ToString());
        }
        private void SetHours()
        {
            for (int i = 10; i < 18; i++)
                HourManComboBox.Items.Add(i.ToString());
        }
        private void SetMinute()
        {
            for (int i = 0; i <= 45; i += 15)
                MinuteManComboBox.Items.Add(i.ToString());
        }
        private void SetData(string name, string surname, string pass)
        {
            manager = new Manager(name, surname, pass);
            master_collection = new MasterCollection();
            master_history = new MasterHistory();         
            appointment_collection = new AppointmentCollection();
            start_session = DateTime.Now;
            FileStream fs = new FileStream("masterd.dat", FileMode.OpenOrCreate);
            master_collection.GetFromDatFile(fs);
            fs.Close();
            master_history.History.Push(master_collection.SaveState());
            StreamReader sr = new StreamReader("appointments.txt");
            appointment_collection.GetFromFile(sr);
            sr.Close();
            for( int i=0; i<appointment_collection.Size; i++)
            {
                AllProceduresList.Items.Add(appointment_collection[i].ToString());
            }
            sr = new StreamReader("money.txt");
            money = double.Parse(sr.ReadLine());
            sr.Close();

            manager.SetCommand(new MasterAddCommand(master_collection));
        }
        private void TakeOnButton_Click(object sender, RoutedEventArgs e)
        {
            manager.TakeOnMaster(new Master(NameEmployeeTextBox.Text, SurnameEmployeeTextBox.Text, int.Parse(ExpirienceEmployeeTextBox.Text)));
        }
        private void Window_Closed(object sender, EventArgs e)
        {           
            if( MessageBox.Show("Save changes about masters?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No )
            {
                master_collection.RestoreState(master_history.History.Pop());
            }          
            StreamWriter sw = new StreamWriter("masters.txt");
            master_collection.SaveToFile(sw);           
            sw.Close();
            FileStream fs = new FileStream("masterd.dat", FileMode.OpenOrCreate);
            master_collection.SaveToDatFile(fs);
            fs.Close();
            sw = new StreamWriter("report.txt");
            DateTime end_session = DateTime.Now;        
            TimeSubSystem ts = new TimeSubSystem(start_session, end_session);
            PasswordSubSystem ps = new PasswordSubSystem(access_password);
            ReportSystem r = new ReportSystem(ts, ps);
            manager.MakeReport(r, sw);
            sw.Close();
            if( boss != null )
            {
                StreamWriter swcopy = new StreamWriter("moneyreportcopy.txt");
                boss.GetMoneyReport(money_report, swcopy);
                swcopy.Close();
            }
        }
        private void GetMoneyButton_Click(object sender, RoutedEventArgs e)
        {
            if( PasswordTextBox.Text == access_money_password )
            {             
                StreamWriter sw = new StreamWriter("moneyreport.txt");
                money_report = new MoneyReport( new NoTakenReport(), money, money);
                boss = new Boss(manager.Name, manager.Surname, access_money_password);
                boss.GetMoneyReport(money_report, sw);
                money = money_report.HowMuchNow;
                sw.Close();
                StreamWriter sw1 = new StreamWriter("money.txt");
                sw1.WriteLine(money_report.HowMuchNow.ToString());
                sw1.Close();
            }
        }
    }
}
