﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;

namespace BeautySalonMAPZ
{
    class Manager
    {
        ICommand command;
        public string Name { get; set; }
        public string Surname { get; set; }
        public Password Password { get; set; }
        public Manager( string name, string surname, string password )
        {
            Name = name;
            Surname = surname;
            this.GetPassword(password);
        }
        private void GetPassword(string pass)
        {
            Password = Password.getInstance(pass);
        }
        public void MakeReport( ReportSystem rs, StreamWriter sw )
        {
            sw.WriteLine("Manager: " + Name + " " + Surname);
            rs.MakeReport(sw);
        }
        public void SetCommand( ICommand com )
        {
            command = com;
        }
        public void TakeOnMaster( Master master )
        {
            command.Execute(master);
        }
    }
    class Password
    {
        private static Password instance;
        public string Value { get; private set; }
        private Password(string val)
        {
            this.Value = val;
        }
        public static Password getInstance(string val)
        {
            if (instance == null)
                instance = new Password(val);
            return instance;
        }
    }

    interface ICommand
    {
        void Execute(Master master);
    }
    class MasterAddCommand: ICommand
    {
        MasterCollection mc;
        public MasterAddCommand( MasterCollection masterCollection )
        {
            mc = masterCollection;
        }
        public void Execute ( Master master )
        {
            mc.AddMaster(master);
        }
    }
}
