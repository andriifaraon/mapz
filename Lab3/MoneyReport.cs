﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BeautySalonMAPZ
{
    class MoneyReport: Report
    {
        private string to_file;
        public string BossName { get; set; }
        public string BossSurname { get; set; }
        public string BossPassword { get; set; }
        public double HowMuchNow { get; set; }
        public double HowMuchTake { get; set; }
        public DateTime DateTimeNow { get; set; }
        public IMoneyReportState State { get; set; }
        public MoneyReport( IMoneyReportState state, double how_much_now, double how_much_take)
        {
            HowMuchNow = how_much_now;
            HowMuchTake = how_much_take;
            State = state;
        }
        public override void FormReport()
        {
            to_file = State.GetMoney(this);
        }
        public override void SaveToFile(StreamWriter sw)
        {
            sw.WriteLine(to_file);
        }
    }
    interface IMoneyReportState
    {
        string GetMoney(MoneyReport money_report );
    }
    class AlreadyTakenReport : IMoneyReportState
    {
        public string GetMoney(MoneyReport money_report )
        {
            return "Report (copy)\n" + "Boss: " + money_report.BossName + " " + money_report.BossSurname +
                "\nPassword: " + money_report.BossPassword + "\nDate: " + money_report.DateTimeNow +
                "\nRest: " + money_report.HowMuchNow + "\nTaken: " + money_report.HowMuchTake;
        }
    }
    class NoTakenReport : IMoneyReportState
    {
        public string GetMoney(MoneyReport money_report)
        {
            money_report.DateTimeNow = DateTime.Now;
            money_report.HowMuchNow -= money_report.HowMuchTake;
            money_report.State = new AlreadyTakenReport();
            return "Report\n" + "Boss: " + money_report.BossName + " " + money_report.BossSurname +
                "\nPassword: " + money_report.BossPassword + "\nDate: " + money_report.DateTimeNow +
                "\nRest: " + money_report.HowMuchNow + "\nTaken: " + money_report.HowMuchTake;
        }
    }

    abstract class Report
    {
        public void MakeReport( StreamWriter sw )
        {
            FormReport();
            SaveToFile(sw);
        }
        public abstract void FormReport();
        public abstract void SaveToFile(StreamWriter sw);
    }
}
