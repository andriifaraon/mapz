﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BeautySalonMAPZ
{
    class Boss
    {       
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Password { get; set; }
        public Boss( string name, string surname, string password)
        {
            Name = name;
            Surname = surname;
            Password = password;
        }
        public void GetMoneyReport( MoneyReport mr , StreamWriter sw )
        {
            mr.BossName = Name;
            mr.BossSurname = Surname;
            mr.BossPassword = Password;
            mr.MakeReport(sw);
        }
    }
}
