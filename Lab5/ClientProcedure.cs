﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeautySalonMAPZ
{
    class Procedure: ICloneable
    {
        public ProcedureType TypeProcedure { get; set; }
        public PolishType Polish { get; set; }
        public bool IsClean { get; set; }
        public double Price { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("");
            if (TypeProcedure == ProcedureType.Manicure)
                sb.Append("Manicure with ");
            else
                sb.Append("Pedicure with ");
            if( Polish == PolishType.Gel )
                sb.Append("gel polish ");
            else
                sb.Append("no gel polish ");
            if (IsClean)
                sb.Append("with clean, ");
            else
                sb.Append("without clean, ");
            sb.Append("price: " + Price.ToString());
            return sb.ToString();
        }
        public object Clone()
        {
            return new Procedure() { TypeProcedure = this.TypeProcedure, Polish = this.Polish, IsClean = this.IsClean, Price = this.Price };
        }
    }
    abstract class ProcedureBuilder
    {
        public Procedure ClientProcedure { get; private set; }
        public void CreateProcedure()
        {
            ClientProcedure = new Procedure();
        }
        public abstract void SetTypeProcedure();
        public abstract void SetPolish();
        public abstract void SetPrice();
        public abstract void SetIsClean();
    }
    class ManicGelClean : ProcedureBuilder
    {
        public override void SetTypeProcedure() {
            this.ClientProcedure.TypeProcedure = ProcedureType.Manicure;
        }
        public override void SetPolish() {
            this.ClientProcedure.Polish = PolishType.Gel;
        }
        public override void SetIsClean() {
            this.ClientProcedure.IsClean = true;
        }
        public override void SetPrice() {
            this.ClientProcedure.Price = 450;
        }
    }
    class ManicGelNoClean : ProcedureBuilder
    {
        public override void SetTypeProcedure()
        {
            this.ClientProcedure.TypeProcedure = ProcedureType.Manicure;
        }
        public override void SetPolish()
        {
            this.ClientProcedure.Polish = PolishType.Gel;
        }
        public override void SetIsClean()
        {
            this.ClientProcedure.IsClean = false;
        }
        public override void SetPrice()
        {
            this.ClientProcedure.Price = 350;
        }
    }
    class ManicNoGelClean : ProcedureBuilder
    {
        public override void SetTypeProcedure()
        {
            this.ClientProcedure.TypeProcedure = ProcedureType.Manicure;
        }
        public override void SetPolish()
        {
            this.ClientProcedure.Polish = PolishType.NoGel;
        }
        public override void SetIsClean()
        {
            this.ClientProcedure.IsClean = true;
        }
        public override void SetPrice()
        {
            this.ClientProcedure.Price = 300;
        }
    }
    class ManicNoGelNoClean : ProcedureBuilder
    {
        public override void SetTypeProcedure()
        {
            this.ClientProcedure.TypeProcedure = ProcedureType.Manicure;
        }
        public override void SetPolish()
        {
            this.ClientProcedure.Polish = PolishType.NoGel;
        }
        public override void SetIsClean()
        {
            this.ClientProcedure.IsClean = false;
        }
        public override void SetPrice()
        {
            this.ClientProcedure.Price = 200;
        }
    }
    class PadicGelClean : ProcedureBuilder
    {
        public override void SetTypeProcedure()
        {
            this.ClientProcedure.TypeProcedure = ProcedureType.Padicure;
        }
        public override void SetPolish()
        {
            this.ClientProcedure.Polish = PolishType.Gel;
        }
        public override void SetIsClean()
        {
            this.ClientProcedure.IsClean = true;
        }
        public override void SetPrice()
        {
            this.ClientProcedure.Price = 550;
        }
    }
    class PadicGelNoClean : ProcedureBuilder
    {
        public override void SetTypeProcedure()
        {
            this.ClientProcedure.TypeProcedure = ProcedureType.Padicure;
        }
        public override void SetPolish()
        {
            this.ClientProcedure.Polish = PolishType.Gel;
        }
        public override void SetIsClean()
        {
            this.ClientProcedure.IsClean = false;
        }
        public override void SetPrice()
        {
            this.ClientProcedure.Price = 450;
        }
    }
    class PadicNoGelClean : ProcedureBuilder
    {
        public override void SetTypeProcedure()
        {
            this.ClientProcedure.TypeProcedure = ProcedureType.Padicure;
        }
        public override void SetPolish()
        {
            this.ClientProcedure.Polish = PolishType.NoGel;
        }
        public override void SetIsClean()
        {
            this.ClientProcedure.IsClean = true;
        }
        public override void SetPrice()
        {
            this.ClientProcedure.Price = 400;
        }
    }
    class PadicNoGelNoClean : ProcedureBuilder
    {
        public override void SetTypeProcedure()
        {
            this.ClientProcedure.TypeProcedure = ProcedureType.Padicure;
        }
        public override void SetPolish()
        {
            this.ClientProcedure.Polish = PolishType.NoGel;
        }
        public override void SetIsClean()
        {
            this.ClientProcedure.IsClean = false;
        }
        public override void SetPrice()
        {
            this.ClientProcedure.Price = 300;
        }
    }
}
