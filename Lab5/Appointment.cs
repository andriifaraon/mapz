﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;

namespace BeautySalonMAPZ
{
    class Appointment
    {
        Procedure procedure;
        Master master;
        public string ClientName { get; set; }
        public string ClientSurname { get; set; }
        public DateTime FirstDate { get; set; }
        public DateTime SecondDate { get; set; }      
        public double AppointmentPrice { get { return procedure.Price; } }
        public Appointment()
        {
            procedure = null; master = null;
            ClientName = ""; ClientSurname = "";
            FirstDate = DateTime.Now; SecondDate = DateTime.Now;
        }
        public Appointment(Procedure p, Master m, Client c, DateTime d1, double time)
        {
            procedure = (Procedure)p.Clone();
            master = (Master)m.Clone();
            ClientName = c.Name;
            ClientSurname = c.Surname;
            FirstDate = d1;
            SecondDate = FirstDate.AddHours(time);
        }
        public Appointment(Procedure p, Master m, string name, string surname, DateTime d1, DateTime d2)
        {
            procedure = (Procedure)p.Clone();
            master = (Master)m.Clone();
            ClientName = name;
            ClientSurname = surname;
            FirstDate = d1;
            SecondDate = d2;
        }
        public void SaveToFile( StreamWriter sw )
        {
            sw.WriteLine(procedure.TypeProcedure);
            sw.WriteLine(procedure.Polish);
            sw.WriteLine(procedure.IsClean);
            sw.WriteLine(procedure.Price.ToString());
            sw.WriteLine(master.Name);
            sw.WriteLine(master.Surname);
            sw.WriteLine(ClientName);
            sw.WriteLine(ClientSurname);
            sw.WriteLine(FirstDate.ToString());
            sw.WriteLine(SecondDate.ToString());
        }
        public static Appointment GetFromFile(StreamReader sr)
        {
            string type_procedure = sr.ReadLine();
            string polish = sr.ReadLine();
            string is_clean = sr.ReadLine();
            string price = sr.ReadLine();
            string master_name = sr.ReadLine();
            string master_surname = sr.ReadLine();
            string client_name = sr.ReadLine();
            string client_surname = sr.ReadLine();
            string first_date = sr.ReadLine();
            string second_date = sr.ReadLine();
            Procedure p_temp = new Procedure();
            Master m_temp = new Master() { Name = master_name, Surname = master_surname };
            if (type_procedure == "Manicure" && polish == "Gel" )
                p_temp = new Procedure() { TypeProcedure= ProcedureType.Manicure, Polish = PolishType.Gel ,
                                        IsClean = bool.Parse(is_clean), Price = double.Parse(price)};
            else if (type_procedure == "Manicure" && polish == "NoGel")
                p_temp = new Procedure() { TypeProcedure= ProcedureType.Manicure, Polish = PolishType.NoGel ,
                                        IsClean = bool.Parse(is_clean), Price = double.Parse(price)};
            else if (type_procedure == "Padicure" && polish == "Gel")
                p_temp = new Procedure() { TypeProcedure= ProcedureType.Padicure, Polish = PolishType.Gel ,
                                        IsClean = bool.Parse(is_clean), Price = double.Parse(price)};
            else if (type_procedure == "Padicure" && polish == "NoGel")
                p_temp = new Procedure() { TypeProcedure= ProcedureType.Padicure, Polish = PolishType.NoGel ,
                                        IsClean = bool.Parse(is_clean), Price = double.Parse(price)};
            return new Appointment(p_temp, m_temp, client_name, client_surname, Convert.ToDateTime(first_date), Convert.ToDateTime(second_date));

        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("");
            sb.Append(procedure.ToString());
            sb.Append(" for " + ClientName + " " + ClientSurname + " by " + master.ToString());
            sb.Append(" " + FirstDate.Month.ToString() + "." +
                FirstDate.Day.ToString() + " " + FirstDate.Hour.ToString() + ":" + FirstDate.Minute.ToString());
            sb.Append(" - " + SecondDate.Month.ToString() + "." +
                SecondDate.Day.ToString() + " " + SecondDate.Hour.ToString() + ":" + SecondDate.Minute.ToString());
            return sb.ToString();
        }
    }
    class AppointmentCollection
    {
        List<Appointment> list_of_appointment;
        public AppointmentCollection()
        {
            list_of_appointment = new List<Appointment>();
        }
        public int Size
        {
            get { return list_of_appointment.Count; }
        }
        public void AddAppointment(Appointment appointment)
        {
            list_of_appointment.Add(appointment);
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("");
            foreach (Appointment a in list_of_appointment)
                sb.Append(a.ToString() + "\n\n");
            return sb.ToString();
        }
        public void SaveToFile(StreamWriter sw)
        {
            sw.WriteLine(list_of_appointment.Count);
            foreach (Appointment a in list_of_appointment)
            {
                a.SaveToFile(sw);
            }
        }
        public void GetFromFile(StreamReader sr)
        {
            int size = int.Parse(sr.ReadLine());
            for( int i=0; i<size; i++)
            {
                list_of_appointment.Add(Appointment.GetFromFile(sr));
            }
        }
        public Appointment this[int index]
        {
            get
            {
                return list_of_appointment[index];
            }
            set
            {
                list_of_appointment[index] = value;
            }
        }
        public void CopyByName( AppointmentCollection appointment_collection_source, string name, string surname )
        {
            for( int i=0; i<appointment_collection_source.Size; i++)
            {
                if( appointment_collection_source[i].ClientName == name && appointment_collection_source[i].ClientSurname == surname)
                {
                    list_of_appointment.Add(appointment_collection_source[i]);
                }
            }
        }
        public void DeleteByName( string name, string surname )
        {       
            list_of_appointment.RemoveAll(j => (j.ClientName == name) && (j.ClientSurname == surname));
        }
    }
}
