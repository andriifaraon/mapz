﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeautySalonMAPZ
{
    abstract class Discount
    {
        public string Name { get; protected set; }
        public Discount(string n)
        {
            this.Name = n;
        }
        public abstract double GetPercent();
    }
    class PromocodeDiscount : Discount
    {
        public PromocodeDiscount() : base("Promocode discount")
        { }
        public override double GetPercent()
        {
            return 2;
        }
    }
    abstract class DiscountDecorator : Discount
    {
        protected Discount discount;
        public DiscountDecorator(string n, Discount discount) : base(n)
        {
            this.discount = discount;
        }
    }
    class AprilPromocode : DiscountDecorator
    {
        public AprilPromocode(Discount d)
            : base(d.Name + ", with April promocode", d)
        { }
        public override double GetPercent()
        {
            return discount.GetPercent() + 1;
        }
    }
    class BlackPromocode : DiscountDecorator
    {
        public BlackPromocode(Discount d)
            : base(d.Name + ", with Black promocode", d)
        { }
        public override double GetPercent()
        {
            return discount.GetPercent() + 4;
        }
    }
    class FacebookPromocode : DiscountDecorator
    {
        public FacebookPromocode(Discount d)
            : base(d.Name + ", with Facebook promocode", d)
        { }
        public override double GetPercent()
        {
            return discount.GetPercent() + 2;
        }
    }
}
