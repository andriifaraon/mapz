﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace BeautySalonMAPZ
{
    /// <summary>
    /// Interaction logic for ClientWindow.xaml
    /// </summary>
    public partial class ClientWindow : Window
    {
        Client client;
        MasterCollection master_collection;
        ClientCollection client_collection;
        AppointmentCollection client_appointment_collection;
        AppointmentCollection appointment_collection;
        private double money = 0;
        public ClientWindow()
        {
            InitializeComponent();
            SetComboBoxes();
        }
        public ClientWindow(string name, string surname)
        {
            InitializeComponent();
            SetComboBoxes();
            SetData(name, surname);            
        }
        private void FormClient( string name, string surname )
        {
            if (DateTime.Now.Month > 2 && DateTime.Now.Month < 6)
                client = new Client(name, surname, new ClientWithSpringDiscount(), new ClientPaying());
            else if (DateTime.Now.Month == 12 || DateTime.Now.Month < 3)
                client = new Client(name, surname, new ClientWithWinterDiscount(), new ClientPaying());
            else
                client = new Client(name, surname, new ClientWithoutDiscount(), new ClientPaying());
        }
        private void SetComboBoxes()
        {
            SetProcedures();
            SetPolish();
            SetMonth();
            SetDays();
            SetHours();
            SetMinute();
        }
        private void SetProcedures()
        {
            ProcedureComboBox.Items.Add("Manicure");
            ProcedureComboBox.Items.Add("Pedicure");
        }
        private void SetPolish()
        {
            PolishComboBox.Items.Add("Gel");
            PolishComboBox.Items.Add("No gel");
        }
        private void SetMonth()
        {
            for (int i = 0; i < 12; i++)
            {
                if (i >= DateTime.Now.Month - 1)
                    MonthComboBox.Items.Add((int)(Months)Enum.GetValues(typeof(Months)).GetValue(i) + 1);
            }
        }
        private void SetDays()
        {
            for (int i = 1; i <=31; i++)
                DayComboBox.Items.Add(i.ToString());
        }
        private void SetHours()
        {
            for (int i = 10; i<18; i++)
            HourComboBox.Items.Add(i.ToString());
        }
        private void SetMinute()
        {
            for (int i = 0; i <= 45; i+=15)
                MinuteComboBox.Items.Add(i.ToString());
        }
        private void SetData( string name, string surname )
        {
            FormClient(name, surname);
            master_collection = new MasterCollection();
            client_collection = new ClientCollection();
            appointment_collection = new AppointmentCollection();
            client_appointment_collection = new AppointmentCollection();
            StreamReader sr = new StreamReader("masters.txt");
            StreamReader sr_c = new StreamReader("clients.txt");
            StreamReader sr_a = new StreamReader("appointments.txt");
            client_collection.GetFromFile(sr_c);
            master_collection.GetFromFile(sr);
            appointment_collection.GetFromFile(sr_a);
            client_appointment_collection.CopyByName(appointment_collection, client.Name, client.Surname);
            appointment_collection.DeleteByName(client.Name, client.Surname);
            if (client.IsMaster(master_collection))
            {
                client.IPayingStrategy = new MasterAsClientPaying();
            }               
            for( int i=0; i < client_appointment_collection.Size; i++)
            {
                ProceduresList.Items.Add(client_appointment_collection[i].ToString());            
            }
            master_collection.DeleteByName(client.Name, client.Surname);
            IMasterIterator iterator = master_collection.CreateIterator();
            while (iterator.HasNext())
            {
                Master master = iterator.Next();
                MasterComboBox.Items.Add(master.ToString());
            }
            sr.Close();
            sr_c.Close();
            sr_a.Close();
        }
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            int procedure_index = ProcedureComboBox.SelectedIndex;
            int polishindex = PolishComboBox.SelectedIndex;
            Procedure procedure = new Procedure();
            Appointment appointment = new Appointment();
            Master master = master_collection[MasterComboBox.SelectedIndex];
            DateTime d1 = new DateTime(2020, int.Parse(MonthComboBox.SelectedItem.ToString()),
                int.Parse(DayComboBox.SelectedItem.ToString()), int.Parse(HourComboBox.SelectedItem.ToString()),
                int.Parse(MinuteComboBox.SelectedItem.ToString()), 0);
            if ( (procedure_index == 0) && (polishindex == 0) && (IsCleanCheckBox.IsChecked == true) )
            {
                procedure = client.MakeAppointment(new ManicGelClean());
                appointment = new Appointment(procedure, master, client, d1, 2.5);
            }
            if ((procedure_index == 0) && (polishindex == 1) && (IsCleanCheckBox.IsChecked == true))
            {
                procedure = client.MakeAppointment(new ManicNoGelClean());
                appointment = new Appointment(procedure, master, client, d1, 2.5);
            }
            if ((procedure_index == 0) && (polishindex == 0) && (IsCleanCheckBox.IsChecked == false))
            {
                procedure = client.MakeAppointment(new ManicGelNoClean());
                appointment = new Appointment(procedure, master, client, d1, 2);
            }
            if ((procedure_index == 0) && (polishindex == 1) && (IsCleanCheckBox.IsChecked == false))
            {
                procedure = client.MakeAppointment(new ManicNoGelNoClean());
                appointment = new Appointment(procedure, master, client, d1, 2);
            }
            if ((procedure_index == 1) && (polishindex == 0) && (IsCleanCheckBox.IsChecked == true))
            {
                procedure = client.MakeAppointment(new PadicGelClean());
                appointment = new Appointment(procedure, master, client, d1, 3);
            }
            if ((procedure_index == 1) && (polishindex == 1) && (IsCleanCheckBox.IsChecked == true))
            {
                procedure = client.MakeAppointment(new PadicNoGelClean());
                appointment = new Appointment(procedure, master, client, d1, 3);
            }
            if ((procedure_index == 1) && (polishindex == 0) && (IsCleanCheckBox.IsChecked == false))
            {
                procedure = client.MakeAppointment(new PadicGelNoClean());
                appointment = new Appointment(procedure, master, client, d1, 2.5);
            }
            if ((procedure_index == 1) && (polishindex == 1) && (IsCleanCheckBox.IsChecked == false))
            {
                procedure = client.MakeAppointment(new PadicNoGelNoClean());
                appointment = new Appointment(procedure, master, client, d1, 2.5);
            }
            string message = "";
            StringBuilder message_messagebox = new StringBuilder("");
            client_appointment_collection.AddAppointment(appointment);
            double percents = GetDiscount(PromocodeTextBox.Text, ref message);
            if (message != "")
                message_messagebox.Append(message);
            message_messagebox.Append("\nSum: ");
            double sum_to_pay = client.Pay(appointment.AppointmentPrice, percents);
            message_messagebox.Append(sum_to_pay.ToString());         
            money += sum_to_pay;
            MessageBox.Show(message_messagebox.ToString(), "Price for appointment", MessageBoxButton.OK, MessageBoxImage.Information);
            ProceduresList.Items.Add(appointment.ToString());
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            StreamReader srm = new StreamReader("money.txt");
            double how_much_now = double.Parse(srm.ReadLine());
            how_much_now += money;
            srm.Close();
            StreamWriter swm = new StreamWriter("money.txt");
            swm.WriteLine(how_much_now);
            swm.Close();
            client_collection.ChangeOrAddClient(client);
            StreamWriter sw = new StreamWriter("clients.txt");
            client_collection.SaveToFile(sw);
            sw.Close();           
            for( int i=0; i<client_appointment_collection.Size; i++)
            {
                appointment_collection.AddAppointment(client_appointment_collection[i]);
            }
            sw = new StreamWriter("appointments.txt");
            appointment_collection.SaveToFile(sw);
            sw.Close();
        }
        private double GetDiscount( string promocode, ref string message )
        {
            double percent = 0;
            Discount discount;
            List<string> promocodes = new List<string>() { "dc_april", "dc_black", "dc_blackapril" , "dc_face", "dc1" };
            if (promocodes.FindIndex((i) => i == promocode) != -1)
            {
                discount = new PromocodeDiscount();
                if (promocode == "dc_april")
                    discount = new AprilPromocode(discount);
                else if (promocode == "dc_black")
                    discount = new BlackPromocode(discount);
                else if (promocode == "dc_face")
                    discount = new FacebookPromocode(discount);
                else if (promocode == "dc_blackapril")
                {
                    discount = new AprilPromocode(discount);
                    discount = new BlackPromocode(discount);
                }
                percent = discount.GetPercent();
                message = "You have: " + discount.Name + " with " + percent.ToString() + " percents";
            }
            return percent;
        }
        private void CardCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CardTextBox.Visibility = Visibility.Visible;
        }
        private void CardCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            CardTextBox.Visibility = Visibility.Hidden;
        }
    }
}
