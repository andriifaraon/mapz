﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BeautySalonMAPZ
{
    interface IPayingProcedure
    {
        double Execute(double sum);
    }
    class ClientPayingProcedure : IPayingProcedure
    {
        public double Execute(double sum)
        {
            return sum;
        }
    }
    class MasterPayingProcedure
    {
        public double ExecuteWithDiscount(double sum)
        {
            double special_sum = 0.98 * sum;
            MessageBox.Show("You have special discount for master" , "Discount", MessageBoxButton.OK, MessageBoxImage.Information);
            return special_sum;
        }
    }
    class MasterToCliaenAdapter : IPayingProcedure
    {
        MasterPayingProcedure master_paying_procedure;
        public MasterToCliaenAdapter(MasterPayingProcedure m)
        {
            master_paying_procedure = m;
        }
        public double Execute(double sum)
        {
            return master_paying_procedure.ExecuteWithDiscount(sum);
        }
    }
}
