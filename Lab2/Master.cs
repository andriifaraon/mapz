﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Runtime.Serialization.Formatters.Binary;

namespace BeautySalonMAPZ
{
    [Serializable]
    class Master : ICloneable
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Experience { get; }
        public Master() { }
        public Master( string name, string surname, int experience )
        {
            Name = name;
            Surname = surname;
            Experience = experience;
        }
        public void SaveToFile( StreamWriter sw )
        {
            sw.WriteLine(Name);
            sw.WriteLine(Surname);
            sw.WriteLine(Experience.ToString());
        }
        public static Master GetFromFile( StreamReader sr )
        {
            string name = sr.ReadLine();
            string surname = sr.ReadLine();
            string experience = sr.ReadLine();
            return new Master(name, surname, int.Parse(experience));
        }
        public object Clone()
        {
            return new Master(this.Name, this.Surname, this.Experience);
        }
        public override string ToString()
        {
            return Name + " " + Surname;
        }
    }
    class MasterCollection: IMasterNumerable
    {
        List<Master> list_of_master;
        public int Size
        {
            get
            {
                return list_of_master.Count;
            }
        }
        public MasterCollection()
        {
            list_of_master = new List<Master>();
        }
        public void AddMaster(Master master)
        {
            list_of_master.Add(master);
        }
        public void SaveToFile(StreamWriter sw)
        {
            foreach (Master m in list_of_master)
            {
                m.SaveToFile(sw);
            }
        }
        public void GetFromFile(StreamReader sr)
        {
            while (!sr.EndOfStream)
            {
                list_of_master.Add(Master.GetFromFile(sr));
            }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("");
            foreach (Master m in list_of_master)
            {
                sb.Append(m.ToString() + "\n");
            }
            return sb.ToString();
        }
        public Master this[int index]
        {
            get
            {
                return list_of_master[index];
            }
            set
            {
                list_of_master[index] = value;
            }
        }
        public void DeleteByName(string name, string surname)
        {
            for (int i = 0; i < list_of_master.Count; i++)
            {
                if (list_of_master[i].Name == name && list_of_master[i].Surname == surname)
                {
                    list_of_master.RemoveAt(i);
                    break;
                }                
            }
        }
        public MasterCollectionMemento SaveState()
        {
            return new MasterCollectionMemento(this.list_of_master);
        }
        public void RestoreState(MasterCollectionMemento memento)
        {
            list_of_master = memento.ListOfMaster.GetRange(0, memento.ListOfMaster.Count);
        }
        public IMasterIterator CreateIterator()
        {
            return new MasterIterator(this);
        }
        public void SaveToDatFile(FileStream fs)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(fs, list_of_master);
        }
        public void GetFromDatFile(FileStream fs)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            list_of_master = (List<Master>)formatter.Deserialize(fs);
        }
    }
    class MasterCollectionMemento
    {
        public List<Master> ListOfMaster { get; private set; }
        public MasterCollectionMemento( List<Master> list_of_master )
        {
            ListOfMaster = list_of_master.GetRange(0, list_of_master.Count);
        }
    }
    class MasterHistory
    {
        public Stack<MasterCollectionMemento> History { get; private set; }
        public MasterHistory()
        {
            History = new Stack<MasterCollectionMemento>();
        }
    }

    //Iterator
    interface IMasterIterator
    {
        bool HasNext();
        Master Next();
    }
    interface IMasterNumerable
    {
        IMasterIterator CreateIterator();
        int Size { get; }
        Master this[int index] { get; }
    }
    class MasterIterator : IMasterIterator
    {
        MasterCollection aggregate;
        int index = 0;
        public MasterIterator(MasterCollection mc)
        {
            aggregate = mc;
        }
        public bool HasNext()
        {
            return index < aggregate.Size;
        }
        public Master Next()
        {
            return aggregate[index++];
        }
    }
}
