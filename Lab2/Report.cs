﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BeautySalonMAPZ
{
    class ReportSystem: Report
    {
        private TimeSubSystem time_system;
        private PasswordSubSystem password_system;
        public ReportSystem(TimeSubSystem ts, PasswordSubSystem ps)
        {
            time_system = ts;
            password_system = ps;
        }
        public override void FormReport()
        {
            time_system.CreateForSaving();
            password_system.CreateForSaving();
        }
        public override void SaveToFile( StreamWriter sw )
        {
            time_system.SaveToFile(sw);
            password_system.SaveToFile(sw);
        }
    }
    class TimeSubSystem
    {
        private DateTime first_date;
        private DateTime second_date;
        private string to_file;
        public TimeSubSystem(DateTime d1, DateTime d2)
        {
            first_date = d1;
            second_date = d2;
        }
        public void CreateForSaving()
        {
            to_file = "Start session: " + first_date.ToString() + ", end session: " + second_date.ToString();
        }
        public void SaveToFile( StreamWriter sw )
        {
            sw.WriteLine(to_file);
        }
    }
    class PasswordSubSystem
    {
        private string system_password;
        private string to_file;
        public PasswordSubSystem(string password)
        {
            system_password = password;
        }
        public void CreateForSaving()
        {
            to_file = "System password: " + system_password;
        }
        public void SaveToFile(StreamWriter sw)
        {
            sw.WriteLine(to_file);
        }
    }
}
