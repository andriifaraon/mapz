﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BeautySalonMAPZ
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly string access_password;
        public MainWindow()
        {
            InitializeComponent();
            access_password = "1234";
        }
        private void EnterButton_Click(object sender, RoutedEventArgs e)
        {
            if (PasswordTextBox.Text == access_password)
            {
                ManagerWindow mw = new ManagerWindow(NameTextBox.Text, SurnameTextBox.Text, PasswordTextBox.Text);
                mw.TitleOfManger.Text = NameTextBox.Text + " " + SurnameTextBox.Text + ", all procedures";
                mw.Show();
            }
            else
            {
                ClientWindow cw = new ClientWindow(NameTextBox.Text, SurnameTextBox.Text);
                cw.TitleOfClient.Text = NameTextBox.Text + " " + SurnameTextBox.Text + ", your procedures:";
                cw.Show();
            }               
        }
    }
}
