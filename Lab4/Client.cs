﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;

namespace BeautySalonMAPZ
{
    class Client : ICloneable
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int HowManyTimes { get; set; }
        public IPaying IPayingStrategy { private get; set; }
        private SeasonalDiscount start_discount;
        private ProcedureBuilder procedure_builder;
        public Client()
        {
            Name = "anon";
            Surname = "anon";
            HowManyTimes = 0;
        }
        public Client(string name, string surname, int how_many )
        {
            Name = name;
            Surname = surname;
            HowManyTimes = how_many;
        }
        public Client( string name, string surname, ClientFactory client_factory, IPaying ipaying  )
        {
            Name = name;
            Surname = surname;
            HowManyTimes = 0;
            IPayingStrategy = ipaying;
            start_discount = client_factory.CreateDiscount();
        }
        public double Pay( double sum, double percent)
        {
            double sum_with_discount = IPayingStrategy.PayForAppointment(sum - sum * percent / 100);
            return start_discount.UseDiscount(sum_with_discount);
        }
        public Procedure MakeAppointment(ProcedureBuilder procedureBuilder)
        {
            procedure_builder = procedureBuilder;
            procedure_builder.CreateProcedure();
            procedure_builder.SetTypeProcedure();
            procedure_builder.SetPolish();
            procedure_builder.SetIsClean();
            procedure_builder.SetPrice();
            HowManyTimes++;
            return  procedure_builder.ClientProcedure;
        }
        public void SaveToFile( StreamWriter sw )
        {
            sw.WriteLine(Name);
            sw.WriteLine(Surname);
            sw.WriteLine(HowManyTimes.ToString());
        }
        public static Client GetFromFile(StreamReader sr)
        {
            string name = sr.ReadLine();
            string surname = sr.ReadLine();
            string how_many = sr.ReadLine();
            return new Client(name, surname, int.Parse(how_many));
        }
        public override string ToString()
        {
            return Name + " " + Surname + " " + HowManyTimes.ToString();
        }
        public override bool Equals(object obj)
        {
            Client temp = obj as Client;
            if (temp.Name == Name && temp.Surname == Surname)
                return true;
            else
                return false;
        }
        public object Clone()
        {
            Client temp = new Client(this.Name, this.Surname, this.HowManyTimes);
            temp.start_discount = this.start_discount;
            return temp;
        }
        public bool IsMaster( MasterCollection mc )
        {
            for (int i = 0; i < mc.Size; i++)
            {
                if (mc[i].Name == this.Name && mc[i].Surname == this.Surname)
                    return true;
            }
            return false;
        }
    }
    abstract class ClientFactory
    {
        public abstract SeasonalDiscount CreateDiscount();
    }
    class ClientWithSpringDiscount : ClientFactory
    {
        public override SeasonalDiscount CreateDiscount()
        {
            return new SpringSeasonalDiscount();
        }
    }
    class ClientWithWinterDiscount : ClientFactory
    {
        public override SeasonalDiscount CreateDiscount()
        {
            return new WinterSeasonalDiscount();
        }
    }
    class ClientWithoutDiscount : ClientFactory
    {
        public override SeasonalDiscount CreateDiscount()
        {
            return new NoSeasonalDiscount();
        }
    }
    abstract class SeasonalDiscount
    {
        public abstract double UseDiscount(double sum);
    }
    class SpringSeasonalDiscount : SeasonalDiscount
    {
        public override double UseDiscount(double sum)
        {
            return 0.98 * sum;
        }
    }
    class WinterSeasonalDiscount : SeasonalDiscount
    {
        public override double UseDiscount(double sum)
        {
            return 0.96*sum;
        }
    }
    class NoSeasonalDiscount : SeasonalDiscount
    {
        public override double UseDiscount(double sum)
        {
            return sum;
        }
    }
    class ClientCollection
    {
        List<Client> list_of_client;
        public ClientCollection()
        {
            list_of_client = new List<Client>();
        }
        public int Size
        {
            get { return list_of_client.Count; }
        }
        public void ChangeOrAddClient(Client client)
        {
            bool flag = true;
            for( int i=0; i<list_of_client.Count; i++)
            {
                if(list_of_client[i].Equals(client))
                {
                    list_of_client[i].HowManyTimes  += client.HowManyTimes;
                    flag = false;
                    break;
                }
            }
            if (flag == true)
                list_of_client.Add(client);
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("");
            foreach (Client c in list_of_client)
                sb.Append(c.ToString() + "\n");
            return sb.ToString();
        }
        public void SaveToFile( StreamWriter sw )
        {
            foreach (Client c in list_of_client)
            {
                c.SaveToFile(sw);
            }
        }
        public void GetFromFile(StreamReader sr)
        {
            while (!sr.EndOfStream)
            {
                list_of_client.Add(Client.GetFromFile(sr));
            }
        }
    }
    interface IPaying
    {
        double PayForAppointment(double sum);
    }
    class ClientPaying: IPaying
    {
        public double PayForAppointment( double sum )
        {
            return sum;
        }
    }
    class MasterAsClientPaying: IPaying
    {
        public double PayForAppointment(double sum)
        {
            double special_sum = 0.98 * sum;
            MessageBox.Show("You have special discount for master", "Discount", MessageBoxButton.OK, MessageBoxImage.Information);
            return special_sum;
        }
    }
}
