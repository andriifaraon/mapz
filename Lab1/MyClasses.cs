﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Interpretetor
{
    interface IExpression
    {
        string Interpret(Context context);
    }
    class Context
    {
        List<string> name;
        List<string> value;
        public Context()
        {
            name = new List<string>();
            value = new List<string>();;
        }
        public string GetVariable(string _name)
        {
            for (int i = 0; i < name.Count; i++)
            {
                if (name[i] == _name)
                    return value[i];
            }
            MessageBox.Show("Variable " +  _name + " does not exist in current context");
            System.Environment.Exit(1);
            return "";
        }
        public void SetVariable(string _name, string _value)
        {
            bool flag = true;
            for (int i = 0; i < name.Count; i++)
            {
                if (name[i] == _name)
                {
                    value[i] = _value;
                    flag = false;
                }
            }
            if (flag == true)
            {
                name.Add(_name);
                value.Add(_value);
            }
        }
    }
    class VariableExpression : IExpression
    {
        string name;
        public VariableExpression(string variableName)
        {
            name = variableName;
        }
        public string Interpret(Context context)
        {
            return context.GetVariable(name);
        }
        
    }
    class ConstantExpression : IExpression
    {
        string value;
        public ConstantExpression( string stringValue )
        {
            value = stringValue;
        }
        public string Interpret( Context context )
        {
            return value;
        }
    }
    class AddExpression : IExpression
    {
        IExpression leftOp;
        IExpression rightOp;
        public AddExpression( IExpression left, IExpression right )
        {
            leftOp = left;
            rightOp = right;
        }
        public string Interpret ( Context context )
        {
            return leftOp.Interpret(context) + rightOp.Interpret(context);
        }
    }
    class PrintExpression : IExpression
    {
        IExpression toPrint;
        public PrintExpression(IExpression Print)
        {
            toPrint = Print;
        }
        public string Interpret(Context context)
        {
            return toPrint.Interpret(context);
        }
    }
    class FindFirstExpression : IExpression
    {
        IExpression inOp;
        IExpression whatOp;
        public FindFirstExpression( IExpression _in, IExpression _what )
        {
            inOp = _in;
            whatOp = _what;
        }
        public string Interpret( Context context )
        {
            string index = (inOp.Interpret(context)).IndexOf(whatOp.Interpret(context)).ToString();
            return index;
        }
    }
    class FindLastExpression : IExpression
    {
        IExpression inOp;
        IExpression whatOp;
        public FindLastExpression(IExpression _in, IExpression _what)
        {
            inOp = _in;
            whatOp = _what;
        }
        public string Interpret(Context context)
        {
            string index = (inOp.Interpret(context)).LastIndexOf(whatOp.Interpret(context)).ToString();
            return index;
        }
    }
    class RemoveExpression : IExpression
    {
        IExpression inOp;
        IExpression whatOp;
        public RemoveExpression(IExpression _in, IExpression _what)
        {
            inOp = _in;
            whatOp = _what;
        }
        public string Interpret(Context context)
        {
            string rez = (inOp.Interpret(context)).Replace(whatOp.Interpret(context), "");
            return rez;
        }
    }
    class CompareExpression: IExpression
    {
        IExpression leftOp;
        IExpression rightOp;
        public CompareExpression(IExpression left, IExpression right)
        {
            leftOp = left;
            rightOp = right;
        }
        public string Interpret(Context context)
        {
            return String.Compare(leftOp.Interpret(context) , rightOp.Interpret(context)).ToString();
        }
    }

    class Client
    {
        List<string> codeStrings;
        List<string> wordStrings;
        List<Token> tokens;
        public Client()
        {
            codeStrings = new List<string>();
        }
        public void LexCode( string code, Context context, TextBox textBox, TextBox forTree )
        {
            string[] separator = { Environment.NewLine };
            codeStrings = code.Split(separator, StringSplitOptions.RemoveEmptyEntries).ToList();
            int flag = 0;
            for( int i=0; i<codeStrings.Count; i++ )
            {
                flag = LexString(codeStrings[i], context, textBox, forTree);
                if( flag < 0 )
                {
                    int count = flag * (-1);
                    if (codeStrings[i + 1] == "{" && i < codeStrings.Count - 1)
                    {
                        int firstblock = i + 1;
                        int nextBlock = 0;
                        int correct = 0;
                        for (int j = i + 2; j < codeStrings.Count; j++)
                        {
                            if (codeStrings[j] == "{") { MessageBox.Show("There must not be insertion blocks"); System.Environment.Exit(1); }
                            if (codeStrings[j] == "}")
                            {
                                correct = 1;
                                nextBlock = j;
                                break;
                            }
                        }
                        if (correct == 0)
                        {
                            MessageBox.Show("Blocks must be closed");
                            System.Environment.Exit(1);
                        }
                        for( int l=0; l<count-1; l++)
                        {
                            for( int f =firstblock; f<nextBlock; f++ )
                            {
                                LexString(codeStrings[f], context, textBox, forTree);
                            }
                        }
                        //forTree.Text += Environment.NewLine + "---------------------" + Environment.NewLine;
                    }
                }
                if( flag == 0 || flag == 1 )
                {
                    if( codeStrings[i+1] == "{" && i < codeStrings.Count-1 )
                    {
                        int firstblock = i + 1;
                        int nextBlock = 0;
                        int correct = 0;
                        for( int j=i+2; j<codeStrings.Count; j++ )
                        {
                            if( codeStrings[j] == "{" ) { MessageBox.Show("There must not be insertion blocks"); System.Environment.Exit(1); }
                            if( codeStrings[j] == "}" )
                            {
                                correct = 1;
                                nextBlock = j;
                                break;
                            }
                        }
                        if( correct == 0 )
                        {
                            MessageBox.Show("Blocks must be closed");
                            System.Environment.Exit(1);
                        }
                        if( flag == 0 )
                        {
                            i = i + nextBlock - firstblock;
                            if (nextBlock + 1 < codeStrings.Count)
                            {
                                if (codeStrings[nextBlock + 1] == "any")
                                {
                                    if (codeStrings[nextBlock + 2] != "{")
                                    {
                                        MessageBox.Show("There must be block after ANY");
                                        System.Environment.Exit(1);
                                    }
                                    for (int j = nextBlock + 3; j < codeStrings.Count; j++)
                                    {
                                        if (codeStrings[j] == "{") { MessageBox.Show("There must not be insertion blocks"); System.Environment.Exit(1); }
                                        if (codeStrings[j] == "}")
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if( flag == 1 )
                        {
                            int first = nextBlock + 2;
                            int last = 0;
                            if(nextBlock + 1 < codeStrings.Count )
                            {
                                if (codeStrings[nextBlock + 1] == "any" )
                                {
                                    if (codeStrings[nextBlock + 2] != "{")
                                    {
                                        MessageBox.Show("There must be open block after ANY");
                                        System.Environment.Exit(1);
                                    }
                                    for (int j = nextBlock + 3; j < codeStrings.Count; j++)
                                    {
                                        if (codeStrings[j] == "{") { MessageBox.Show("There must not be insertion block"); System.Environment.Exit(1); }
                                        if (codeStrings[j] == "}")
                                        {
                                            last = j;
                                            break;
                                        }
                                    }
                                    codeStrings.RemoveRange(first, last - first);
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("There must be open block after WHEN");
                        System.Environment.Exit(1);
                    }
                }
            }
        }
        public int LexString( string s , Context context, TextBox textBox, TextBox forTree)
        {            
            wordStrings = new List<string>();
            tokens = new List<Token>();
            wordStrings = s.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            for( int j=0; j<wordStrings.Count; j++)
            {
                    tokens.Add(new Token(wordStrings[j]));
                    //MessageBox.Show(tokens[j].type.ToString()); 
            }
            if (tokens.Count == 1 && tokens[0].type != Tokens.any  && tokens[0].type != Tokens.block)
            {
                MessageBox.Show("Invalid operation");
                System.Environment.Exit(1);
            }
            if (tokens[0].type == Tokens.variable)
            {
                for( int k=2; k<tokens.Count-1; k++)
                {
                    if( tokens[k].type == Tokens.plus )
                    {
                        if (tokens[k - 1].type != Tokens.value && tokens[k - 1].type != Tokens.variable)
                        {
                            MessageBox.Show("Invalid operation");
                            System.Environment.Exit(1);                           
                        }
                        if (tokens[k + 1].type != Tokens.value && tokens[k + 1].type != Tokens.variable)
                        {
                            MessageBox.Show("Invalid operation");
                            System.Environment.Exit(1);
                        }
                    }
                }
                List<string> pref = new List<string>();
                List<string> toDraw = new List<string>();
                for( int i=1; i<tokens.Count; i++)
                {
                    if( tokens[i].type == Tokens.value || tokens[i].type == Tokens.plus )
                    {
                        IExpression expression = new ConstantExpression(tokens[i].data);
                        pref.Add(expression.Interpret(context));
                        if (tokens[i].type == Tokens.value)
                            toDraw.Add("CE:" + tokens[i].data);
                        else
                            toDraw.Add(tokens[i].data);
                    }
                    else if (tokens[i].type == Tokens.variable)
                    {
                        IExpression expression = new VariableExpression(tokens[i].data);
                        pref.Add(expression.Interpret(context));
                        toDraw.Add("VE:" + tokens[i].data);
                    }
                    else
                        System.Environment.Exit(1);
                }
                forTree.Text += "Set variable "+ tokens[0].data + Environment.NewLine;
                pref = Parser.GetPref(pref);
                toDraw = Parser.GetPref(toDraw);
                Parser.Draw(toDraw, forTree);
                forTree.Text += Environment.NewLine;
                //StringBuilder sb = new StringBuilder("");
                //foreach (string sy in toDraw)
                //    sb.Append(sy + " ");
                //MessageBox.Show(sb.ToString());
                context.SetVariable(tokens[0].data, Parser.GetString(pref, context));
                return 12;
            }
            if( tokens[0].type == Tokens.print )
            {
                for (int k = 2; k < tokens.Count - 1; k++)
                {
                    if (tokens[k].type == Tokens.plus)
                    {
                        if (tokens[k - 1].type != Tokens.value && tokens[k - 1].type != Tokens.variable)
                        {
                            MessageBox.Show("Invalid operation");
                            System.Environment.Exit(1);
                        }
                        if (tokens[k + 1].type != Tokens.value && tokens[k + 1].type != Tokens.variable)
                        {
                            MessageBox.Show("Invalid operation");
                            System.Environment.Exit(1);
                        }
                    }
                }
                List<string> pref = new List<string>();
                List<string> toDraw = new List<string>();
                for (int i = 1; i < tokens.Count; i++)
                {
                    if (tokens[i].type == Tokens.value || tokens[i].type == Tokens.plus)
                    {
                        IExpression expression = new ConstantExpression(tokens[i].data);
                        pref.Add(expression.Interpret(context));
                        if (tokens[i].type == Tokens.value)
                            toDraw.Add("CE:" + tokens[i].data);
                        else
                            toDraw.Add(tokens[i].data);
                    }
                    else if (tokens[i].type == Tokens.variable)
                    {
                        IExpression expression = new VariableExpression(tokens[i].data);
                        pref.Add(expression.Interpret(context));
                        toDraw.Add("VE:" + tokens[i].data);
                    }
                    else
                        System.Environment.Exit(1);
                }
                forTree.Text += "Print string "  + Environment.NewLine;
                toDraw = Parser.GetPref(toDraw);
                Parser.Draw(toDraw, forTree);
                forTree.Text += Environment.NewLine;
                pref = Parser.GetPref(pref);
                IExpression exp = new PrintExpression(new ConstantExpression(Parser.GetString(pref, context)));
                textBox.Text += exp.Interpret(context) + Environment.NewLine;
                return 12;
            }
            if( tokens[0].type == Tokens.reg)
            {
                if( tokens[tokens.Count-1].type != Tokens.manage ) { MessageBox.Show("Invalid operation"); System.Environment.Exit(1); }
                int findex1 = 1;
                int lindex1 = 1;
                int findex2 = 2;
                int lindex2 = 2;
                List<string> toDraw1 = new List<string>();
                List<string> toDraw2 = new List<string>();
                List<string> pref1 = new List<string>();
                List<string> pref2 = new List<string>();
                for ( int i = 2; i<tokens.Count; i++)
                {
                    if( (tokens[i].type == Tokens.value || tokens[i].type == Tokens.variable) )
                    {
                        if((tokens[i-1].type == Tokens.value || tokens[i-1].type == Tokens.variable) )
                        {
                            lindex1 = i - 1;
                            break;
                        }
                    }
                }
                findex2 = lindex1 + 1;
                for (int i = lindex1+1; i < tokens.Count; i++)
                {
                    if ( tokens[i].type == Tokens.manage )
                    {
                        lindex2 = i - 1;
                    }
                }
                for (int i = findex1; i < lindex1+1; i++)
                {
                    if (tokens[i].type == Tokens.value || tokens[i].type == Tokens.plus)
                    {
                        IExpression expression = new ConstantExpression(tokens[i].data);
                        pref1.Add(expression.Interpret(context));
                        if (tokens[i].type == Tokens.value)
                            toDraw1.Add("CE:" + tokens[i].data);
                        else
                            toDraw1.Add(tokens[i].data);
                    }
                    else if (tokens[i].type == Tokens.variable)
                    {
                        IExpression expression = new VariableExpression(tokens[i].data);
                        pref1.Add(expression.Interpret(context));
                        toDraw1.Add("VE:" + tokens[i].data);
                    }
                    else
                        System.Environment.Exit(1);
                }
                pref1 = Parser.GetPref(pref1);
                for (int i = findex2; i < lindex2 + 1; i++)
                {
                    if (tokens[i].type == Tokens.value || tokens[i].type == Tokens.plus)
                    {
                        IExpression expression = new ConstantExpression(tokens[i].data);
                        pref2.Add(expression.Interpret(context));
                        if (tokens[i].type == Tokens.value)
                            toDraw2.Add("CE:" + tokens[i].data);
                        else
                            toDraw2.Add(tokens[i].data);
                    }
                    else if (tokens[i].type == Tokens.variable)
                    {
                        IExpression expression = new VariableExpression(tokens[i].data);
                        pref2.Add(expression.Interpret(context));
                        toDraw2.Add("VE:" + tokens[i].data);
                    }
                    else
                        System.Environment.Exit(1);
                }
                toDraw1 = Parser.GetPref(toDraw1);
                toDraw2 = Parser.GetPref(toDraw2);
                forTree.Text += "Regular" + Environment.NewLine;
                Parser.Draw(toDraw1, forTree);
                
                pref2 = Parser.GetPref(pref2);
                IExpression left = new ConstantExpression(Parser.GetString(pref1, context));
                IExpression right = new ConstantExpression(Parser.GetString(pref2, context));
                if ( tokens[tokens.Count-1].data == "\\f")
                {                   
                    IExpression expression = new FindFirstExpression(left, right);
                    textBox.Text += expression.Interpret(context) + Environment.NewLine;
                    forTree.Text += Environment.NewLine + "        |" +Environment.NewLine;
                    forTree.Text += "      FindFirstE" + Environment.NewLine;
                    forTree.Text += "         |" + Environment.NewLine;
                }
                if (tokens[tokens.Count - 1].data == "\\l")
                {
                    IExpression expression = new FindLastExpression(left, right);
                    textBox.Text += expression.Interpret(context) + Environment.NewLine;
                    forTree.Text += Environment.NewLine + "         |" + Environment.NewLine;
                    forTree.Text += "      FindLastE" + Environment.NewLine;
                    forTree.Text += "         |" + Environment.NewLine;
                }
                if (tokens[tokens.Count - 1].data == "\\r")
                {                 
                    IExpression expression = new RemoveExpression(left, right);
                    textBox.Text += expression.Interpret(context) + Environment.NewLine;
                    forTree.Text += Environment.NewLine + "          |" + Environment.NewLine;
                    forTree.Text += "      RemovEE" + Environment.NewLine;
                    forTree.Text += "          |" + Environment.NewLine;
                }
                Parser.Draw(toDraw2, forTree);
                forTree.Text += Environment.NewLine;
                return 12;
            }
            if( tokens[0].type == Tokens.when )
            {
                int findex = 0;
                int lindex = 0;
                List<string> firstList = new List<string>();
                for( int i=0; i<tokens.Count; i++)
                {
                    if( tokens[i].type == Tokens.compare || tokens[i].type == Tokens.logic || tokens[i].type == Tokens.when )
                    {
                        firstList.Add(tokens[i].data);
                        findex = i + 1;
                        for( int j=findex; j<tokens.Count; j++)
                        {
                            if (tokens[j].type == Tokens.compare || tokens[j].type == Tokens.logic || j == tokens.Count -1 )
                            {
                                if (j == tokens.Count - 1)
                                    lindex = j;
                                else
                                    lindex = j - 1;
                                break;
                            }
                        }
                        List<string> pref = new List<string>();
                        for (int k = findex; k < lindex+1; k++)
                        {
                            if (tokens[k].type == Tokens.value || tokens[k].type == Tokens.plus)
                            {
                                IExpression expression = new ConstantExpression(tokens[k].data);
                                pref.Add(expression.Interpret(context));
                            }
                            else if (tokens[k].type == Tokens.variable)
                            {
                                IExpression expression = new VariableExpression(tokens[k].data);
                                pref.Add(expression.Interpret(context));
                            }
                            else
                                System.Environment.Exit(1);
                        }
                        pref = Parser.GetPref(pref);
                        firstList.Add(Parser.GetString(pref, context));
                    }
                }
                List<string> pref1 = new List<string>();
                for( int i=1; i<firstList.Count -1; i++)
                {
                    if (firstList[i] == "<" || firstList[i] == ">" || firstList[i] == "=" || firstList[i] == "!=" )
                    {
                        IExpression left = new ConstantExpression(firstList[i - 1]);
                        IExpression right = new ConstantExpression(firstList[i + 1]);
                        IExpression comparer = new CompareExpression(left, right);
                        string rez = comparer.Interpret(context);
                        if (firstList[i] == "<" && rez == "-1")
                            pref1.Add("1");
                        else if (firstList[i] == ">" && rez == "1")
                            pref1.Add("1");
                        else if (firstList[i] == "=" && rez == "0")
                            pref1.Add("1");
                        else if (firstList[i] == "!=" && (rez == "1" || rez == "-1" ))
                            pref1.Add("1");
                        else
                            pref1.Add("0");
                    }
                    if (firstList[i] == "|" || firstList[i] == "&")
                        pref1.Add(firstList[i]);
                }
                List<string> logicPref = new List<string>();
                logicPref = Parser.GetPrefLogic(pref1);
                forTree.Text += "When" + Environment.NewLine;
                Parser.Draw(logicPref, forTree);
                forTree.Text += Environment.NewLine;
                return int.Parse(Parser.GetStringLogic(logicPref, context));
            }
            if( tokens[0].type == Tokens.value )
            {
                MessageBox.Show("Error");
                System.Environment.Exit(1);
            }
            if( tokens[0].type == Tokens.cycle )
            {
                int count;
                if (!int.TryParse(tokens[1].data, out count) || tokens.Count > 2)
                {
                    MessageBox.Show("Invalid operation");
                    System.Environment.Exit(1);
                }
                if ( int.TryParse( tokens[1].data, out count ))
                {
                    if( count > 0 )
                    {
                        forTree.Text += Environment.NewLine + "cycle ( " + count.ToString() + " )" ;
                        forTree.Text += Environment.NewLine + "---------------------" + Environment.NewLine;
                        return (-1) * count;
                    }
                    else
                    {
                        MessageBox.Show("Invalid operation");
                        System.Environment.Exit(1);
                    }
                }
            }
            return 100;
        }       
    }
    class Parser
    {
        public static List<string> GetPref(List<string> list)
        {
            List<string> pref = new List<string>();
            pref.Add(list[0]);
            for (int i = 1; i < list.Count; i++)
            {
                if (list[i] == "+" || list[i] == "-")
                {
                    pref.Add(list[i]);
                }
                else
                {
                    pref.Insert(i - 1, list[i]);
                }
            }
            return pref;
        }
        public static string GetString(List<string> list, Context context )
        {
            if (list.Count == 3)
            {
                if( list[2] == "+" )
                {
                    IExpression exp = new AddExpression(new ConstantExpression(list[0]), new ConstantExpression(list[1]));
                    return exp.Interpret(context);
                }
                return "";
            }
            if (list.Count == 1)
            {
                IExpression exp = new ConstantExpression(list[0]);
                return exp.Interpret(context);
            }
            else
            {
                List<string> temp = new List<string>();
                temp = list.GetRange(3, list.Count - 3);
                IExpression left = new ConstantExpression(list[0]);
                IExpression right = new ConstantExpression(list[1]);
                //temp.Insert(0, list[0] + list[1]);
                temp.Insert(0, new AddExpression(left, right).Interpret(context));
                return GetString(temp, context);
            }
        }
        public static List<string> GetPrefLogic(List<string> list)
        {
            List<string> pref = new List<string>();
            pref.Add(list[0]);
            for (int i = 1; i < list.Count; i++)
            {
                if (list[i] == "&" || list[i] == "|")
                {
                    pref.Add(list[i]);
                }
                else
                {
                    pref.Insert(i - 1, list[i]);
                }
            }
            return pref;
        }
        public static string GetStringLogic(List<string> list, Context context)
        {
            if (list.Count == 3)
            {
                if (list[2] == "|")
                {
                    //if (list[0] == "0" && list[1] == "0")
                    //    return "0";
                    //else
                    //    return "1";
                    IExpression left = new ConstantExpression(list[0]);
                    IExpression right = new ConstantExpression(list[1]);
                    return new OrExpression(left, right).Interpret(context);
                }
                if (list[2] == "&")
                {
                    //if (list[0] == "1" && list[1] == "1")
                    //    return "1";
                    //else
                    //    return "0";
                    IExpression left = new ConstantExpression(list[0]);
                    IExpression right = new ConstantExpression(list[1]);
                    return new AndExpression(left, right).Interpret(context);
                }
                return "";
            }
            if (list.Count == 1)
            {
                return list[0];
            }
            else
            {
                List<string> temp = new List<string>();
                temp = list.GetRange(3, list.Count - 3);
                if( list[2] == "&")
                {
                    IExpression left = new ConstantExpression(list[0]);
                    IExpression right = new ConstantExpression(list[1]);
                    temp.Insert(0, new AndExpression(left, right).Interpret(context));
                    //if (list[0] == "1" && list[1] == "1")
                    //    temp.Insert(0, "1");
                    //else
                    //    temp.Insert(0, "0");
                }
                if (list[2] == "|")
                {
                    //if (list[0] == "0" && list[1] == "0")
                    //    temp.Insert(0, "0");
                    //else
                    //    temp.Insert(0, "1");
                    IExpression left = new ConstantExpression(list[0]);
                    IExpression right = new ConstantExpression(list[1]);
                    temp.Insert(0, new OrExpression(left, right).Interpret(context));
                }
                return GetStringLogic(temp, context);
            }
        }
        public static void Draw( List<string> list, TextBox textBox )
        {
            for (int i = 0; i < list.Count + 3; i++)
                textBox.Text += "  ";
            textBox.Text+=(list[list.Count - 1])+Environment.NewLine;
            if (list.Count != 1)
            {
                for (int i = 0; i < list.Count + 2; i++)
                    textBox.Text += "  ";
                textBox.Text += ("/" + "  " + "\\")+Environment.NewLine;
            }

            for (int i = list.Count - 2; i > 2; i -= 2)
            {
                for (int j = 0; j < i + 3; j++)
                {
                    textBox.Text += "  ";
                }
                textBox.Text +=(list[i - 1] + "      " + list[i]);
                textBox.Text += Environment.NewLine;
                for (int k = 0; k < i + 2; k++)
                {
                    textBox.Text += "  ";
                }
                textBox.Text+=("/" + "  " + "\\")+Environment.NewLine;
                //WriteLine();
            }
            if (list.Count > 1)
                textBox.Text += ("      " + list[0] + "      " + list[1]);
        }
    }
    class Token
    {
        public string data { get; set; }
        public Tokens type { get; set; }
        public Token() { }
        public Token(string s)
        {
            
            if( s == "when" )
            {
                type = Tokens.when;
                data = s;
            }
            else if (s[0] == '\"' && s[s.Length-1] == '\"' )
            {
                type = Tokens.value;
                data = s.Substring(1, s.Length - 2);
            }
            else if( s == "cycle")
            {
                type = Tokens.cycle;
                data = s;
            }
            else if (s == "print")
            {
                type = Tokens.print;
                data = s;
            }
            else if (s == "reg")
            {
                type = Tokens.reg;
                data = s;
            }
            else if( s == "any")
            {
                type = Tokens.any;
                data = s;
            }
            else if( s == "+" )
            {
                type = Tokens.plus;
                data = s;
            }
            else if( s.Length == 2 && s[0]=='\\' && (s[1] == 'r' || s[1] == 'l' || s[1] == 'f'))
            {
                type = Tokens.manage;
                data = s;
            }
            else if ( s.Length == 1 && ( s == "{" || s == "}" ) ) 
            {
                type = Tokens.block;
                data = s;
            }
            else if ( ( s==">" || s=="<" || s=="=" || s=="!=" ))
            {
                type = Tokens.compare;
                data = s;
            }
            else if ( s.Length == 1 && ( s=="&" || s=="|"))
            {
                type = Tokens.logic;
                data = s;
            }
            else
            {
                char[] noInVar = { '\"', '\'', '*', '/', '+', '-' , '\\' , '{' , '}', '&' , '|', '<', '>', '=' };
                for( int i=0; i<s.Length; i++)
                {
                    for( int j=0; j<noInVar.Length; j++ )
                    {
                        if( s[i] == noInVar[j] )
                        {
                            MessageBox.Show("Bad name for variable");
                            System.Environment.Exit(1);
                        }
                    }
                }
                data = s;
                type = Tokens.variable;
            }
        }
    }
    enum Tokens
    {
        when,
        cycle,
        print,
        reg,
        variable,
        value,
        any,
        plus,
        manage,
        block,
        compare,
        logic
    }
    class AndExpression: IExpression
    {
        IExpression leftOp;
        IExpression rightOp;
        public AndExpression( IExpression _left, IExpression _right)
        {
            leftOp = _left;
            rightOp = _right;
        }
        public string Interpret( Context context )
        {
            if (leftOp.Interpret(context) == "1" && rightOp.Interpret(context) == "1")
                return "1";
            else
                return "0";
        }
    }
    class OrExpression : IExpression
    {
        IExpression leftOp;
        IExpression rightOp;
        public OrExpression(IExpression _left, IExpression _right)
        {
            leftOp = _left;
            rightOp = _right;
        }
        public string Interpret(Context context)
        {
            if (leftOp.Interpret(context) == "0" && rightOp.Interpret(context) == "0")
                return "0";
            else
                return "1";
        }
    }
}
